<body>

  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php wp_head(); ?>
  </head>
  
  <div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
      <div class="off-canvas position-left" id="offCanvas" data-off-canvas>

        <!-- Close button -->
        <button class="close-button" aria-label="Close menu" type="button" data-close>
          <span aria-hidden="true">&times;</span>
        </button>

        <!-- Menu -->
        <ul class="vertical menu">
          <li><a href="#">Foundation</a></li>
          <li><a href="#">Dot</a></li>
          <li><a href="#">ZURB</a></li>
          <li><a href="#">Com</a></li>
          <li><a href="#">Slash</a></li>
          <li><a href="#">Sites</a></li>
        </ul>

      </div>

      <div class="off-canvas-content" data-off-canvas-content>
        <!-- Page content -->
      </div>
    </div>
  </div>
</body>