<?php
/*
Template Name: All Images
*/

//randomize initial load
if (isset($wpdb)) {
  $q = "SELECT
    `ID` AS post_id
  FROM
    `{$wpdb->prefix}posts`
  WHERE
    `post_type` = 'attachment'
  AND
    `post_mime_type` = 'image/jpeg'
  ORDER BY
    RAND()
  LIMIT
    80;";

  $images = $wpdb->get_results($q);
}

get_header();
?>

<div id="intro">
	<div class="row">
		<div class="column">
			<h1><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
			<h2><?php bloginfo('description'); ?></h2>
		</div>
	</div>
</div>



<div class="row">
	<div class="column">

		<div id="search">
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			    <label>
			        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
			        <input type="search" class="search-field"
			            placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
			            value="<?php echo get_search_query() ?>" name="s"
			            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
			    </label>
			    <input type="submit" class="search-submit"
			        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
			</form>
		</div>

		<?php get_template_part( 'template-parts/searchtags', get_post_format() ); ?>
    <?php get_template_part('template-parts/grid-toggler');?>

	</div>
</div>




<div class="photos">
	<?php
	$total_attachments = $wpdb->get_var("SELECT COUNT(ID) FROM {$wpdb->prefix}posts WHERE post_type = 'attachment'");

	$n = (int) round($total_attachments / 20);
	kriesi_pagination($n);
	?>

  <div id="circleGBG">
    <div id="circleG">
      <div id="circleG_1" class="circleG"></div>
      <div id="circleG_2" class="circleG"></div>
      <div id="circleG_3" class="circleG"></div>
    </div>
  </div>

  <?php if (!empty($images)) : ?>
    <!--load from query-->
    <div id="igallery">
      <?php foreach ($images as $image) : ?>
        <?php include get_template_directory()."/template-parts/image-card.php"; ?>
      <?php endforeach;?>
    </div>
  <?php else : ?>
    <!--load from plugin-->
    <?php get_template_part( 'template-parts/content', get_post_format() ); ?>
  <?php endif; ?>

  <br />
  <?php //get_template_part('template-parts/loadmore', get_post_format());?>

	<nav class="navigation bottom">

	<div class="next">
		<?php
			$total_attachments = $wpdb->get_var("SELECT COUNT(ID) FROM {$wpdb->prefix}posts WHERE post_type = 'attachment'");
			$total = (int) round($total_attachments / 20);
			$tlessc = $total - $paged;
			$next   = ($total - $tlessc) + 1;
			$prev = $paged - 1;
		?>

		<?php if ( $total > $paged )  { ?>
		<a class="nav-previous button expanded" href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=40&amp;paged=<?php echo $next; ?>">
			Next</a>
		<?php } ?>

			<?php if ( $total == $paged )  { ?>
				<a class="nav-previous button expanded" href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=40&amp;paged=<?php echo $prev; ?>">
					Previous</a>
			<?php } ?>
		</div>

        <?php
        	kriesi_pagination($n);
        ?>

	</nav>
</div>

<?php get_footer(); ?>
