//requires jQuery
var cartName        = 'igallery-cart';
var iGalleryStorage = localStorage;
var iGalleryArray   = {};
var totalKB         = {};

$(document).ready(function() {
  if (iGalleryStorage.getItem(cartName) == null) {
    iGalleryStorage.setItem(cartName, JSON.stringify(iGalleryArray));
  }

  iGalleryArray = JSON.parse(iGalleryStorage.getItem(cartName));

  $('li.addtolightbox > a').click(cartToggle);

  $('#d-all').click(function() {
    var ids = new Array();
    for (var i in iGalleryArray) {
      ids.push(i.replace('title-', ''));
    }

    window.location.href = location.protocol+'//'+location.host+location.pathname+'services?flag=download&images='+ids.join(',');
  });

  buildCart();
});

function addToCart(index, src) {
  iGalleryArray[index] = src;
  iGalleryStorage.setItem(cartName, JSON.stringify(iGalleryArray));
  if ($('#gallery-cart').find('div[data-index='+index+']').length == 0) {
    appendSideCart(index, src);
  }
  updateCartTotal();
  $('#gallery-cart .column:not([data-index])').remove();
}

function removeFromCart(index) {
  delete iGalleryArray[index];
  iGalleryStorage.setItem(cartName, JSON.stringify(iGalleryArray));
  var item = document.querySelector('.column[data-index='+index+']');
  $('#gallery-cart').append($('#gallery-cart').find('.column[data-index='+index+']'));
  $('#gallery-cart').find('.column[data-index='+index+']').hide().removeAttr('data-index');
  $('.item .fancybox[data-title-id='+index+']').prev().find('.minus').removeClass('minus').addClass('add').find('.fa').removeClass('fa-minus').addClass('fa-plus');
  removeSize(index);
  updateCartTotal();
}

function flushCart() {
  iGalleryArray = {};
  iGalleryStorage.setItem(cartName, JSON.stringify(iGalleryArray));
  $('#gallery-cart').html('');
  $('.item .minus .fa').removeClass('fa-minus').addClass('fa-plus');
  $('.item .minus').addClass('add').removeClass('minus');
  totalKB = {};
  updateCartTotal();
}

function buildCart() {
  $('#gallery-cart').html('');
  iGalleryArray = JSON.parse(iGalleryStorage.getItem(cartName));
  for (var i in iGalleryArray) {
    appendSideCart(i, iGalleryArray[i]);
    if ($('.item .fancybox[data-title-id='+i+']').length > 0) {
      $('.item .fancybox[data-title-id='+i+']').prev().find('.add').removeClass('add').addClass('minus').find('.fa').removeClass('fa-plus').addClass('fa-minus');
    }
  }

  updateCartTotal();
}

function appendSideCart(index, src) {
  addSize(index, src);

  if (src.indexOf('-150x150.') < 0) {
    var r     = /(?:\.([^.]+))?$/;
    var re    = /.*?(-\d*x\d*)\..*?/;
    var ext   = r.exec(src)[1];
    var found = re.exec(src);

    if (found !== null && found.length > 1) {
      src = src.replace(found[1], '-150x150');
    } else {
      src = src.replace('.'+ext, '-150x150.'+ext);
    }
  }

  $('#gallery-cart').append('<div class="column" data-index='+index+'>'+
    '<span title="remove" class="remove">'+
    '<a href="javascript:void(0);" onclick="removeFromCart(\''+index+'\');">-</a>'+
    '</span>'+
    '<img src="'+src+'" height="150" />'+
    '</div>');
}

function updateCartTotal() {
  if (iGalleryArray != null) {
    var size = Object.keys(iGalleryArray).length;

    if (size == 0) {
      $('.downloadall').fadeOut('fast');
      $('#itemcount').fadeOut('fast');
      $('.gallery-cart-instruction').show();
    } else {
      $('.downloadall').show();
      $('#itemcount').show();
      $('.gallery-cart-instruction').hide();
    }

    if (size == 1) {
      $('#gallery-cart-total').text(size+' photo');
    } else {
      $('#gallery-cart-total').text(size+' photos');
    }

    $('#itemcount').text(size);
  }
}

function cartToggle() {
  var item = $(this).parent().parent().parent().next();
  var src = $(this).parent().next().find('a').attr('href');

  if ($(this).hasClass('add')) {
    addToCart(item.data('title-id'), src);
    $(this).removeClass('add').addClass('minus');
    $(this).find('.fa').removeClass('fa-plus').addClass('fa-minus');
    cartNotify(this);
  } else {
    removeFromCart(item.data('title-id'));
    $(this).removeClass('minus').addClass('add');
    $(this).find('.fa').removeClass('fa-minus').addClass('fa-plus');
    cartDenotify(this);
  }
}

function cartToggleFromFancybox(elem, id) {
  var gridButton = $('.fancybox[data-title-id='+id+']').prev().find('.addtolightbox > a');
  var image = $('.fancybox-image').attr('src');
  var child = $(elem).find('.fa');

  if (child.hasClass('fa-plus')) {
    addToCart(id, image);
    child.removeClass('fa-plus').addClass('fa-minus');
    gridButton.find('.fa').removeClass('fa-plus').addClass('fa-minus');
    gridButton.removeClass('add').addClass('minus');
  } else {
    removeFromCart(id);
    child.removeClass('fa-minus').addClass('fa-plus');
    gridButton.find('.fa').removeClass('fa-minus').addClass('fa-plus');
    gridButton.removeClass('minus').addClass('add');
  }
}

function validateCartStatus(id) {
  if (iGalleryArray != null && iGalleryArray.hasOwnProperty(id)) {
    return true;
  } else {
    return false;
  }
}

function cartNotify(elem) {
  var lb = $('#lightbox-toggler');
  var t  = Object.keys(iGalleryArray).length;

  $('#lightbox-notif').text('image added');
  if (!lb.parent().hasClass('announce')) {
    lb.parent().addClass('announce');
    turnOffNotif();
  }
}

function cartDenotify(elem) {
  var lb = $('#lightbox-toggler');
  var t  = Object.keys(iGalleryArray).length;

  $('#lightbox-notif').text('image removed');
  if (!lb.parent().hasClass('announce')) {
    lb.parent().addClass('announce');
    turnOffNotif();
  }
}

function turnOffNotif() {
  setTimeout(function() {
    $('.lightboxicon').removeClass('announce');
  }, 1500);
}

function addSize(id, src) {
  var xhr = $.ajax({
    type: 'HEAD',
    url: src,
    success: function(r) {
      totalKB[id] = xhr.getResponseHeader('Content-Length');
      console.log('Size from JS: '+totalKB[id]);
      computeDownloadSize();
    }
  });
}

function removeSize(id) {
  delete totalKB[id];
  computeDownloadSize();
}

function computeDownloadSize() {
  if (totalKB != null) {
    var downloadSize = 0;

    for (var i in totalKB) {
      downloadSize += parseInt(totalKB[i]);
    }

    if (downloadSize != '') {
      var ext = 'kb';
      downloadSize = Math.floor(downloadSize / 1024);

      if (downloadSize.toString().length >= 5) {
        ext = 'mb';
        downloadSize = (downloadSize / 1024).toFixed(2);
      }

      if ($('#d-size').length < 1) {
        $('#d-all').append(' <span id="d-size"></span>');
      }

      $('#d-size').text('('+downloadSize+ext+')');
    } else {
      $('#d-size').remove();
    }
  }
}

function shortcutToTrigger() {
  $(this).parent().parent().find('ul .addtolightbox a').trigger('click');
}

function revealTags(elem) {
  if ($(elem).find('span').html().length <= 0) {
    $.ajax({
      url: '/services/',
      method: 'get',
      data: {
        flag: 'image-tag',
        id: $(elem).data('id')
      },
      success: function(response) {
        if (response.hasOwnProperty('warning')) {
          console.log(response.warning);
        } else {
          console.log($('.item[dataindex='+$(elem).data('id')+']'));
        }
      }
    });
  }
}
