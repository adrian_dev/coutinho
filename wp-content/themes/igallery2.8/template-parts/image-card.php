<?php $info = get_post($image->post_id); ?>

<div class="item" data-index="<?php echo $info->ID; ?>">
  <div class="overlay">
    <ul>
      <li class="addtolightbox">
        <a href="javascript:void(0);" class="add" title="add to lightbox">
          <i class="fa fa-plus" aria-hidden="true"></i>
        </a>
      </li>
      <li class="download">
        <a href="<?php echo reset(wp_get_attachment_image_src($info->ID, 'full')); ?>" download title="Quick download full version">
          <i class="fa fa-download" aria-hidden="true"></i>
        </a>
      </li>
      <li class="toggle-tags" onmouseover="revealTags(this);" data-id="<?php echo $info->ID; ?>">
        <a href="javascript:void(0);">
          <img src="<?php echo get_template_directory_uri().'/assets/images/tag.svg'?>" />
        </a>
        <span></span>
      </li>
    </ul>
  </div>
  <a class="fancybox" rel="gallery1" href="<?php echo reset(wp_get_attachment_image_src($info->ID, 'large')); ?>" data-title-id="title-<?php echo $info->ID; ?>">
    <?php echo wp_get_attachment_image($info->ID, 'grid-thumb'); ?>
  </a>

  <div id="title-<?php echo $info->ID; ?>" class="hide">
    <a href="<?php echo get_attachment_link($info->ID); ?>">
      <i class="fa fa-info-circle" aria-hidden="true"></i>
      <?php echo $info->post_title; ?>
    </a>
    <a href="javascript:void(0);" class="add" title="add to lightbox" onclick="cartToggleFromFancybox(this, 'title-<?php echo $info->ID; ?>')">
      <i class="fa fa-plus" aria-hidden="true"></i>
    </a>
    <a href="<?php echo reset(wp_get_attachment_image_src($info->ID, 'full')); ?>" download title="Quick download full version">
      <i class="fa fa-download" aria-hidden="true"></i>
    </a>
  </div>
</div>
