<div class="row">
  <div class="column text-center" id="grid-toggler">
    <a href="javascript:void(0);" onclick="toggleGallery();" data-status="150" class="button hollow secondary small" title="switch to large thumbnails view">
      <span>thumbnail size:</span>
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/th.svg" height="20" width="20" />
    </a>
  </div>
</div>
<style>
  #grid-toggler .button {
    height: 38px;
    border-radius: 3px;
    border-color: transparent;
    color: #333;
    text-transform: uppercase;
    font-weight: bolder;
    transition: all 220ms;
  }
  #grid-toggler .button span {
    margin-right: 8px;
    vertical-align: middle;
  }
  #grid-toggler .button:hover {
    border-color: #555;
  }
  #grid-toggler .button:active {
    opacity: 0.5;
    transition: all 220ms;
  }
</style>
<script>
var storage = localStorage;

if (storage.getItem('thumb') == 270) {
  $("#grid-toggler a").data("status", 270);
  $("#grid-toggler a img").attr("src", "<?php echo get_template_directory_uri(); ?>/assets/images/th-large.svg");
} else {
  $("#grid-toggler a").data("status", 150);
  $("#grid-toggler a img").attr("src", "<?php echo get_template_directory_uri(); ?>/assets/images/th.svg");
}

function toggleGallery() {
  $("#igallery .item").css("opacity", 0);
  if ($("#grid-toggler a").data("status") == 270) {
    $("#grid-toggler a img").attr("src", "<?php echo get_template_directory_uri(); ?>/assets/images/th.svg");
    $("#grid-toggler a").data("status", 150);
    $('#igallery').justifiedGallery({rowHeight: 150});
    storage.setItem('thumb', 150);
  } else {
    $("#grid-toggler a img").attr("src", "<?php echo get_template_directory_uri(); ?>/assets/images/th-large.svg");
    $("#grid-toggler a").data("status", 270);
    $('#igallery').justifiedGallery({rowHeight: 270});
    storage.setItem('thumb', 270);
  }
}

$(function() {
  $('#igallery').justifiedGallery().on('jg.complete', function(e) {
    $("#igallery .item").animate({opacity:1},220);
  });
});
</script>
