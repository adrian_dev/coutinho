<div id="igallery" >

    <div class="grid-sizer"></div>
    <div class="gutter-sizer"></div>

    <?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
       'posts_per_page' => 20,
       'post_type' => 'attachment',
       'paged'     => $paged,
       'orderby' => 'rand'
      );


      $attachments = get_posts( $args );
         if ( $attachments ) {
            foreach ( $attachments as $attachment ) {
                $image_attributes = wp_get_attachment_image_src( $attachment->ID, 'grid-thumb' );

                $img_large_url = wp_get_attachment_image_src( $attachment->ID, 'large' );
                $aliu = $img_large_url[0]; //attachment large image url

                $img_full_url = wp_get_attachment_image_src( $attachment->ID, 'full' );
                $afiu = $img_full_url[0]; //attachment full image url

                $attachment_title = get_the_title($attachment->ID);

                $imgmeta = wp_get_attachment_metadata( $attachment->ID );

                $attchurl = get_attachment_link( $attachment->ID );

                $meta = $imgmeta['image_meta'];

                $ititle = $meta['aperture'];
        ?>

	<?php include(locate_template('template-parts/overlay.php'));  ?>

       <?php

             }
        }
    ?>

</div>