
    <?php
        $attachment_title = get_the_title(get_the_ID());

        $img_large_url = wp_get_attachment_image_src( get_the_ID(), 'large' );
        $aliu = $img_large_url[0]; //attachment large image url

        $img_full_url = wp_get_attachment_image_src( get_the_ID(), 'full' );
        $afiu = $img_full_url[0]; //attachment full image url

        $attchurl = get_attachment_link( get_the_ID() );
    ?>

	<div class="grid-sizer"></div>
	<div class="gutter-sizer"></div>

    <div class="item">
    	<div class="overlay">
    		<ul>
                <li class="addtolightbox">
                    <a href="javascript:void(0);" class="add" title="add to lightbox">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </a>
                </li>
    			<li class="download">
    				<a href="<?php echo $afiu; ?>" download title="Quick download full version">
    					<i class="fa fa-download" aria-hidden="true"></i>
    				</a>
    			</li>
    		</ul>
    	</div>
    	<a class="fancybox" rel="gallery1" href="<?php echo $aliu; ?>" data-title-id="title-<?php echo the_ID(); ?>">
            <?php the_title(); ?>
            <?php echo wp_get_attachment_image( get_the_ID(), 'grid-thumb' ); ?>
    	</a>

    	<div id="title-<?php the_ID(); ?>" class="hide">
    		<a href="<?php echo $attchurl; ?> "><i class="fa fa-info-circle" aria-hidden="true"></i> <?php the_title(); ?></a>

            <a href="#" class="add" title="add to lightbox">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </a>

    		<a href="<?php echo $afiu; ?>" download title="Quick download full version">
    			<i class="fa fa-download" aria-hidden="true"></i>
    		</a>
    	</div>
    </div>
