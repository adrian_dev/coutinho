  <?php
  /**
   * The template for displaying the header
   *
   * Displays all of the head element and everything up until the "container" div.
   *
   * @package FoundationPress
   * @since FoundationPress 1.0.0
   */

  ?>
  <!doctype html>
  <html class="no-js" <?php language_attributes(); ?> >
    <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

    <div class="off-canvas-wrapper">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvas" data-off-canvas>

          <!-- Close button -->
          <button class="close-button" aria-label="Close menu" type="button" data-close>
            <span aria-hidden="true">&times;</span>
          </button>

          <h3 class="logo">i<span>Gallery</span> <i>tags</i></h3>

          <?php //echo do_shortcode( '[mla_tag_cloud taxonomy=photos_keywords mla_output=ulist]' ); ?>
          <?php get_template_part( 'template-parts/searchtags-tagcloud', get_post_format() ); ?>

        </div>

        <div class="off-canvas-content" data-off-canvas-content>
          <!-- Page content -->

          <header id="masthead" class="site-header" role="banner">
            <div class="title-bar" data-responsive-toggle="site-navigation">
              <button class="menu-icon" type="button" data-toggle="mobile-menu"></button>
              <div class="title-bar-title">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
              </div>
            </div>

            <nav id="site-navigation" class="main-navigation top-bar" role="navigation">

              <div class="top-bar-left">
                <?php foundationpress_top_bar_r(); ?>

                <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'topbar' ) : ?>
                  <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
                <?php endif; ?>
              </div>

              <div class="top-bar-title">
                <ul class="menu">
                  <li class="home"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></li>
                </ul>
              </div>

              <div class="top-bar-right">

                <div class="lightboxicon">
                  <a title="Open lightbox" id="lightbox-toggler" data-open="openLightbox"><i class="fa fa-picture-o" aria-hidden="true"></i> <span id="itemcount">0</span></a>
                    <div id="lightbox-notif">image added</div>
                </div>

              </div>
            </nav>
          </header>

          <div class="reveal" id="openLightbox" data-reveal data-close-on-click="true" data-animation-in="slide-in-right" data-animation-out="slide-out-right">
            
            <div class="row">
              <div class="column">
                <h1>Lightbox</h1>
                <p class="lead">You can download all photos from your lightbox here.</p>
                <p class="label secondary" id="gallery-cart-total">0 photos</p>
              </div>
            </div>

            <div class="row large-up-4" id="gallery-cart"></div>
            <div class="gallery-cart-instruction">Click the <i class="fa fa-plus"></i> icon to add image to the collection</div>

            <div class="downloadall">
              <div class="row">
                <div class="large-6 column">
                  <a id="d-all" class="button" download><i class="fa fa-download" aria-hidden="true"></i> Download lightbox</a>
                </div>

                <div class="large-6 column">
                  <a id="c-all" class="button alert" href="javascript:void(0);" onclick="flushCart();"><i class="fa fa-times" aria-hidden="true"></i> Clear lightbox</a>
                </div>
              </div>
            </div>


            <button class="close-button" data-close aria-label="Close modal" type="button">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          
          <section class="container">
            <?php do_action( 'foundationpress_after_header' ); ?>

