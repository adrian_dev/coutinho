<?php
/**
 * The template for displaying search results pages.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */


//randomize initial load
if (isset($wpdb)) {
  $term = "%{$_GET['s']}%";

  $q = "SELECT
    `post_id`
  FROM
    `{$wpdb->prefix}postmeta`
  WHERE
    `meta_key` = '_wp_attachment_metadata'
  AND
    `meta_value` LIKE '%s'
  ORDER BY
    RAND()
  LIMIT
    30;";

  $images = $wpdb->get_results($wpdb->prepare($q, $term));
}

get_header(); ?>

<div class="search-form">
	<div class="row">
		<div class="column">
			<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
		    <label>
	        <span class="screen-reader-text"><?php echo _x('Search for:', 'label'); ?></span>

	        <input type="search" class="search-field"
            placeholder="<?php echo esc_attr_x('Search …', 'placeholder'); ?>"
            value="<?php echo get_search_query(); ?>" name="s"
            title="<?php echo esc_attr_x('Search for:', 'label'); ?>" />
		    </label>
		    <input type="submit" class="search-submit" value="<?php echo esc_attr_x('Search', 'submit button'); ?>" />
			</form>
		</div>
	</div>
</div>

<?php get_template_part( 'template-parts/searchtags', get_post_format() ); ?>

<?php get_template_part('template-parts/grid-toggler');?>

<div id="circleGBG">
  <div id="circleG">
    <div id="circleG_1" class="circleG"></div>
    <div id="circleG_2" class="circleG"></div>
    <div id="circleG_3" class="circleG"></div>
  </div>
</div>

<div id="igallery" >
  <?php if (!empty($images)) : ?>
    <!--load from query-->
    <?php foreach ($images as $image) : ?>
      <?php include "template-parts/image-card.php"; ?>
    <?php endforeach;?>
  <?php else: ?>
    <!--load from plugin-->
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   	  <?php get_template_part( 'template-parts/content-search', get_post_format() ); ?>
      <?php endwhile; ?>
    <?php else : get_template_part( 'template-parts/content-none', get_post_format() ); ?>
    <?php endif; ?>
  <?php endif; ?>
 </div>

<?php //get_template_part('template-parts/loadmore', get_post_format()); ?>

<?php do_action( 'foundationpress_before_pagination' ); ?>

<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
	<nav id="post-nav">
		<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
		<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
	</nav>
<?php } ?>

<?php do_action( 'foundationpress_after_content' ); ?>

<?php get_footer();
