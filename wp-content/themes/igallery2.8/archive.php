<?php
/**
 * The template for displaying search results pages.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="search-form">
	<div class="row">
		<div class="column">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				    <label>
				        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
				        <input type="search" class="search-field"
				            placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
				            value="<?php echo get_search_query() ?>" name="s"
				            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				    </label>
				    <input type="submit" class="search-submit"
				        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
				</form>
		</div>
	</div>
</div>

<?php get_template_part( 'template-parts/searchtags', get_post_format() ); ?>

<h4 class="text-center">#<?php single_tag_title(); ?> </h4>

<div id="igallery" >

 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

 	<?php get_template_part( 'template-parts/content-search', get_post_format() ); ?>

 <?php endwhile; ?>

<?php else : get_template_part( 'template-parts/content-none', get_post_format() ); ?>

 <?php endif; ?>

 </div>





	<?php do_action( 'foundationpress_before_pagination' ); ?>

	<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>

		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
		</nav>
	<?php } ?>

	<?php do_action( 'foundationpress_after_content' ); ?>


<?php get_footer();
