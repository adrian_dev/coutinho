<?php
/*
Plugin Name: Image Meta Keywords Collection
Description: Collect and store image meta keywords
Code Name: Project Alaba
Author: Adrian Valderama
Version: 0.1
*/
defined('ABSPATH') or die('No script kiddies please!');

define('IMK__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('IMK__TABLE_NAME', 'metakeywords');

add_action('admin_menu', 'image_meta_keywords_collection');
add_action('wp_ajax_sync_metakeywords', 'sync_metakeywords');
register_activation_hook(__FILE__, 'meta_keywords_install');

function image_meta_keywords_collection() {
    add_menu_page('Image Meta Keywords Collection', 'Meta Keywords', 'manage_options', 'meta-keywords-init', 'meta_keywords_init');
}

function meta_keywords_init() {
    require_once(IMK__PLUGIN_DIR.'api.php');

    $api = new IMKAPI();
    
    if (isset($_GET['reset']) && $_GET['reset'] == 1) {
        $api->clear(IMK__TABLE_NAME);
    }

    $api->prepare_items(IMK__TABLE_NAME, 15, (isset($_GET['paged']) ? $_GET['paged'] : 1));

    require_once(IMK__PLUGIN_DIR.'views/dashboard.php');

}

function meta_keywords_install() {
    global $wpdb;

    $version         = '1.0';
    $table_name      = $wpdb->prefix.IMK__TABLE_NAME;
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE ".$table_name." (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        keyword varchar(100) DEFAULT '' NOT NULL,
        total int DEFAULT 0 NOT NULL,
        status tinyint DEFAULT 1 NOT NULL,
        PRIMARY KEY  (id)
    ) ".$charset_collate.";";

    require_once(ABSPATH.'wp-admin/includes/upgrade.php');

    dbDelta($sql);

    add_option('metakeywords_db_version', $version);
}

function sync_metakeywords() {
    global $wpdb;
    
    if (!get_option('last_image_sifted')) {
        add_option('last_image_sifted', 0);
    }

    $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
    $limit  = 50;

    $q = "SELECT
            ID
        FROM
            wp_posts
        WHERE
            post_type = 'attachment'
        AND
            post_status = 'inherit'
        AND
            post_mime_type = 'image/jpeg'
        AND
            ID > %d
        ORDER BY
            ID ASC
        LIMIT
            %d
        OFFSET
            %d;";

    $images   = $wpdb->get_results($wpdb->prepare($q, get_option('last_image_sifted'), $limit, $offset));
    $keywords = array('new' => array(), 'existing' => array());
    foreach ($images as $image) {
        $q = "SELECT
                `meta_value`
            FROM
                `wp_postmeta`
            WHERE
                `meta_key` = '_wp_attachment_metadata'
            AND
                `post_id` = %d;";

        $field = $wpdb->get_row($wpdb->prepare($q, $image->ID));
        $data  = unserialize($field->meta_value);
        foreach ($data['image_meta']['keywords'] as $keyword) {
            $keyword = html_entity_decode($keyword);
            $q = "SELECT
                    `id`, `total`
                FROM
                    `".$wpdb->prefix.IMK__TABLE_NAME."`
                WHERE
                    `keyword` = '%s'";
            if ($item = $wpdb->get_row($wpdb->prepare($q, $keyword))) {
                $wpdb->update(
                    $wpdb->prefix.IMK__TABLE_NAME,
                    array('total' => $item->total + 1),
                    array('id'    => $item->id)
                );
                $keywords['existing'][] = $keyword;
            } else {
                $wpdb->insert($wpdb->prefix.IMK__TABLE_NAME, array(
                    'keyword' => $keyword,
                    'total'   => 1,
                    'status'  => 1
                ));
                $keywords['new'][] = $keyword;
            }
        }

        update_option('last_image_sifted', $image->ID);
    }

    header('Content-Type: application/json');
    echo json_encode($keywords);
    die();
}
