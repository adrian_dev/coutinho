<?php
if (!class_exists('WP_List_Table')) {
	require_once(ABSPATH.'wp-admin/includes/class-wp-list-table.php');
}

class IMKAPI extends WP_List_Table
{
    public function prepare_items($table = 'metakeywords', $per_page = 15, $page_number = 1)
    {
        global $wpdb, $_wp_column_headers;

        $screen = get_current_screen();
        $sql    = "SELECT * FROM ".$wpdb->prefix.$table;

        if (isset($_GET['s']) && !empty($_GET['s'])) {
            $sql .= " WHERE keyword LIKE '%".urldecode($_GET['s'])."%'";
        }

        if (!empty($_REQUEST['orderby'])) {
            $sql .= " ORDER BY ".esc_sql($_REQUEST['orderby']);
            $sql .= !empty($_REQUEST['order']) ? ' '.esc_sql($_REQUEST['order']) : 'ASC';
        }
        
        $count = $wpdb->get_row(str_replace('*', 'COUNT(id) AS total_items', $sql));

        $sql .= " LIMIT ".$per_page;
        $sql .= " OFFSET ".(($page_number - 1) * $per_page);

        $this->_column_headers = array($this->get_columns(), array('id'), $this->get_sortable_columns());
        $this->process_bulk_action($table);
        $this->set_pagination_args(array('total_items' => $count->total_items, 'per_page' => $per_page));
        $this->items = $wpdb->get_results($sql);
        
        

        return $result;
    }

    public function get_columns()
    {
         return array(
             'cb'      => '<input type="checkbox" />',
             'id'      => __('ID'),
             'keyword' => __('Keyword'),
             'total'   => __('Total'),
             'status'  => __('Status')
         );
    }

    public function get_sortable_columns()
    {
        return array(
            'keyword' => array('keyword', true),
            'total'   => array('total', true)
        );
    }

    function column_default($item, $column_name)
    {
        switch($column_name) {
            case 'keyword':
            case 'total':
            case 'status':
                return $item->$column_name;
                break;
            default:
                return print_r($item, true) ; //Show the whole array for troubleshooting purposes
                break;
        }
    }

    public function get_bulk_actions()
    {
        return array(
            'enable'  => __('Enable'),
            'disable' => __('Disable'),
        );
    }
    
    public function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="id[]" value="%s" />', $item->id);    
    }

    public function column_status($item)
    {
        if ($item->status) {
            return sprintf('<span class="dashicons dashicons-yes"></span>');
        } else {
            return sprintf('<span class="dashicons dashicons-no-alt"></span>');
        }
    }

    /*public function column_keyword($item)
    {
        $actions = array(
            'toggle' => $item->status ? sprintf('<a href="">Disable</a>') : sprintf('<a href="">Enable</a>'),
            'view'   => '<a href="">View Images</a>'
        );

        return sprintf('%1$s %2$s', $item->keyword, $this->row_actions($actions));
    }*/
    
    public function process_bulk_action($table)
    {
        global $wpdb;

        if ('enable' === $this->current_action()) {
            $tick = 1;
        } else if ('disable' === $this->current_action()) {
            $tick = 0;
        }

        if (isset($_GET['id']) && is_array($_GET['id'])) {
            foreach ($_GET['id'] as $id) {
                $wpdb->query($wpdb->prepare("UPDATE ".$wpdb->prefix.$table." SET status = %d WHERE id = %d", $tick, $id));
            }
        }
    }
    
    public function clear($table)
    {
        global $wpdb;

        $wpdb->query("DELETE FROM ".$wpdb->prefix.$table);
        
        if (!get_option('last_image_sifted')) {
            add_option('last_image_sifted', 0);
        } else {
            update_option('last_image_sifted', 0);
        }
    }
}