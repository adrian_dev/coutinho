<div class="wrap">
    <h1>
        <img src="<?php echo plugin_dir_url(__FILE__);?>icon.jpg" height="33" style="vertical-align:middle;" />
        <?php esc_html_e( 'Image Meta Keywords Collection' , 'image meta keywords collection');?>
    </h1>
    <hr class="wp-header-end">
    <br />
    <div id="col-left">
        <a href="javascript:void(0);" id="sync-metakeywords" class="button">Update Keywords Pool</a>
        <p>Note: Update may take long while to finish depending on how much images are already stored in iGallery.</p>
        <p><small><em>Meta keywords cannot be manually edited or deleted.</em></small></p>
        <ul id="response" style="height:500px;width:100%;overflow-y:scroll;"></ul>
        <a href="javascript:void(0);" id="clear-all" style="color:#a00;">Clear All Keywords Found</a>
    </div>
    <div id="col-right">
        <form method="get" action="admin.php">
            <input type="hidden" name="page" value="meta-keywords-init" />
            <?php $api->search_box('search', 'keyword');?>
            <?php $api->display();?>
        </form>
    </div>
</div>

<script>
jQuery(document).ready(function() {
    jQuery('#sync-metakeywords').click(function() {
        syncKeywords();
    });

    jQuery('#clear-all').click(function() {
        if (confirm('Are you sure you want to clear everything?')) {
            window.location.href = '<?php echo get_admin_url();?>admin.php?page=meta-keywords-init&reset=1';
        }
    });
});

function syncKeywords() {
    jQuery.ajax({
        url: ajaxurl,
        data: {action: 'sync_metakeywords'},
        success: function(response) {
            var div = jQuery('#response');
            console.info('sync request done');

            response.new.forEach(function(data) {
                div.append('<li>New: '+data+'</li>');
            });

            response.existing.forEach(function(data) {
                div.append('<li>Updated: '+data+'</li>');
            });

            if (response.new.length > 0 || response.existing.length > 0) {
                syncKeywords();
            } else {
                div.append('<li>Sync complete</li>');
            }
        }
    });
}
</script>