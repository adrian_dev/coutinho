<?php
/*
Plugin Name: Mass Tagging
Description: Mass Meta Assignment
Code Name: Project Koke
Author: Adrian Valderama
Version: 1.0.0
*/
defined('ABSPATH') or die('No script kiddies please!');

require_once "Andromeda.class.php";

add_action('admin_menu', 'mass_meta_andromeda');
add_action('wp_ajax_more_imgs', 'more_imgs');
add_action('wp_ajax_save_tags', 'save_tags');

function mass_meta_andromeda() {
  add_menu_page(
    'Mass Meta Andromeda',
    'Image Tagging',
    'edit_posts',
    'andromeda-init',
    'andromeda_init'
  );
}

function andromeda_init() {
  $andromeda = new Andromeda();

  if (!empty($_POST) && array_key_exists("save", $_POST)) {
    $response = $andromeda->warp();
  } else {
    $response = false;
  }

  require_once "views/andromeda.php";
}

function more_imgs() {
  $andromeda = new Andromeda();
  $images    = $andromeda->more();

  header('Content-Type: application/json');
  echo json_encode($images);

  wp_die();
}

function save_tags() {
  $andromeda = new Andromeda();
  echo $andromeda->warp();

  wp_die();
}
