<?php
if (!class_exists('WP_List_Table')) {
	require_once(ABSPATH.'wp-admin/includes/class-wp-list-table.php');
}

class Andromeda extends WP_List_Table
{
  private $tblMeta = "postmeta";
  private $tblImgs = "posts";
  private $tblTags = "metakeywords";
  private $limit = 50;

  public function get()
  {
    global $wpdb;

    $q = "SELECT
        *
      FROM
        `{$wpdb->prefix}{$this->tblImgs}`
      WHERE
        `post_type` = 'attachment'
      AND
        `post_status` = 'inherit'
      AND
        `post_mime_type` = 'image/jpeg'
      ORDER BY
        `ID` DESC
      LIMIT
        %d
      OFFSET
        %d;";

    $images = $wpdb->get_results($wpdb->prepare($q, 100, 0));

    return $images;
  }

  public function tags()
  {
    global $wpdb;

    $q    = "SELECT * FROM `{$wpdb->prefix}{$this->tblTags}`;";
    $tags = $wpdb->get_results($q);

    return $tags;
  }

  public function warp()
  {
    global $wpdb;

    $keywords = array();
    $success = false;

    if (array_key_exists("new_tag", $_POST)) {
      foreach ($_POST["new_tag"] as $keyword) {
        $q = "SELECT
          `id`
        FROM
          `{$wpdb->prefix}{$this->tblTags}`
        WHERE
          LOWER(`keyword`) = LOWER('$keyword');";

        $result = $wpdb->get_row($q);

        if (!empty($result)) {
          $wpdb->insert($wpdb->prefix.$this->tblTags, array(
            "keyword" => $keyword,
            "total" => count($_POST["image"])
          ));
        }
      }

      $keywords = $_POST["new_tag"];
    }

    if (array_key_exists("tag", $_POST)) {
      foreach ($_POST["tag"] as $tag) {
        $q = "SELECT
          `keyword`
        FROM
          `{$wpdb->prefix}{$this->tblTags}`
        WHERE
          `id` = '{$tag}';";

        $result = $wpdb->get_row($q);
        $keywords[] = $result->keyword;
      }
    }

    if (array_key_exists("image", $_POST)) {
      foreach ($_POST["image"] as $image) {
        $q = "SELECT
          *
        FROM
          `{$wpdb->prefix}{$this->tblMeta}`
        WHERE
          `post_id` = '{$image}'
        AND
          `meta_key` = '_wp_attachment_metadata';";

        $result = $wpdb->get_row($q);
        $values = unserialize($result->meta_value);
        $diff = array_diff($values["image_meta"]["keywords"], $keywords);

        $values["image_meta"]["keywords"] = array_merge($diff, $keywords);
        $meta = serialize($values);

        if ($wpdb->update($wpdb->prefix.$this->tblMeta, array("meta_value" => $meta), array("meta_id" => $result->meta_id))) {
          $success = true;
        }
      }
    }

    return $success;
  }

  public function more()
  {
    global $wpdb;

    $offset = $_POST["offset"];
    $q = "SELECT
        *
      FROM
        `{$wpdb->prefix}{$this->tblImgs}`
      WHERE
        `post_type` = 'attachment'
      AND
        `post_status` = 'inherit'
      AND
        `post_mime_type` = 'image/jpeg'
      ORDER BY
        `ID` DESC
      LIMIT
        %d
      OFFSET
        %d;";

    $images = $wpdb->get_results($wpdb->prepare($q, $this->limit, $offset));

    return $images;
  }
}
