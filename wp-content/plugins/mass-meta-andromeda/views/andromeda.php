<?php
  $images = $andromeda->get();
  $tags = $andromeda->tags();
?>
<div class="wrap">
  <?php if ($response): ?>
    <div class="message">Images successfully tagged.</div>
  <?php endif;?>
  <h1>Image Meta Keywords Mass Assignment</h1>
  <form method="post" action="">
    <div class="console">
      <div class="new">
        <input type="text" placeholder="new tag" />
        <a href="javascript:void(0);" class="page-title-action">+</a>
      </div>
      <ul class="tags">
        <?php foreach ($tags as $tag): ?>
          <li>
            <label for="tag-<?php echo $tag->id; ?>" class="button tag">
              <input type="checkbox" name="tag[]" onchange="toggleTag(this)" id="tag-<?php echo $tag->id; ?>" value="<?php echo $tag->id; ?>" />
              <?php echo $tag->keyword; ?>
            </label>
          </li>
        <?php endforeach; ?>
      </ul>
      <div class="finish">
        <a href="javascript:void(0);" class="button button-primary button-hero"><center>SAVE</center></a>
      </div>
    </div>
    <ul class="andromeda-grid">
      <?php foreach ($images as $image): ?>
        <?php $thumbnail = wp_get_attachment_image_src($image->ID, 'thumbnail'); ?>
        <li><label for="img-<?php echo $image->ID; ?>"><img src="<?php echo $thumbnail[0]; ?>" title="<?php echo $image->post_title; ?>" alt="<?php echo $image->post_title; ?>" width="103" /></label><input type="checkbox" onchange="toggleImg(this)" id="img-<?php echo $image->ID; ?>" name="image[]" value="<?php echo $image->ID; ?>" /></li>
      <?php endforeach; ?>
    </ul>
  </form>

  <div class="aftermath">
    <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0h24v24H0z" fill="none"/>
      <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"/>
    </svg>
    Images successfully tagged.
  </div>
</div>

<style>
  .wrap h1 {
    position: fixed;
    width: 100%;
    z-index: 4;
    background: #f1f1f1;
    top: 32px;
    padding-bottom: 27px;
  }
  .andromeda-grid {
    width: 70%;
    display: inline-block;
    float: right;
    font-size: 0px;
    margin-top: 55px;
  }
  .andromeda-grid li {
    display: inline-block;
    padding: 0px;
    margin: 0px;
    position: relative;
    box-sizing: border-box;
    margin: 1px;
  }
  .andromeda-grid li:not(.highlight):hover label img {
    opacity: 0.8
  }
  .andromeda-grid li label {
    display: inline-block;
  }
  .andromeda-grid li input[type=checkbox] {
    position: absolute;
    right: 5px;
    top: 10px;
    display: none;
  }
  .andromeda-grid li:hover input[type=checkbox] {
    display: block;
  }
  .andromeda-grid li img {
    display: block;
  }
  .tags {
    height: 400px;
    overflow-y: auto;
  }
  .tags li {
    display: inline-block;
    margin: 2px !important;
    font-size: 1em;
  }
  .tag input[type=checkbox] {
    display: none;
  }
  .new {
    padding: 0px 0px 3px 0px;
    vertical-align: middle;
  }
  .new input[type=text] {
    width: 100%;
  }
  .new input[type=text]::-webkit-input-placeholder {
    color: #aaa;
    font-style: italic;
  }
  .new input[type=text]::-moz-placeholder {
    color: #aaa;
    font-style: italic;
  }
  .new input[type=text]:-ms-input-placeholder {
    color: #aaa;
    font-style: italic;
  }
  .new input[type=text]:-moz-placeholder {
    color: #aaa;
    font-style: italic;
  }
  .new .page-title-action {
    position: absolute;
    top: 6px;
    right: 4px;
    padding: 0px;
    font-size: 20px;
    line-height: 21px;
    height: 25px;
    width: 22px;
    text-align: center;
  }
  .highlight {
    background-color: #000;
  }
  .highlight img {
    opacity: 0.2;
  }
  .highlight input[type=checkbox] {
    display: block !important;
  }
  .message {
    background: lightgoldenrodyellow;
    padding: 20px;
    color: brown;
    font-size: 14px;
    display: inline-block;
    border: 1px solid brown;
  }
  .console {
    font-size: 0px;
    width: 23%;
    display: inline-block;
    position: fixed;
    border: 1px solid #ccc;
    padding: 5px;
    background-color: #fff;
    z-index: 3;
    top: 97px;
  }
  .finish {
    padding: 5px 0px 3px 0px;
  }
  .finish .button {
    width: 100%;
    box-sizing: border-box;
  }
  .aftermath {
    position: fixed;
    top: 40px;
    right: 8px;
    background-color: #7bbd6b;
    padding: 10px;
    border: 1px solid #999;
    border-radius: 3px;
    font-size: 16px;
    z-index: 4;
    display: none;
    width: 386px;
    text-align: center;
    font-weight: bolder;
  }
  .aftermath svg {
    vertical-align: middle;
  }
</style>

<script>
  var offset  = 0;
  var shifted = false;
  var win     = jQuery(window);
  var maxed   = 50;

  jQuery(function() {
    offset = jQuery(".andromeda-grid li").length;

    jQuery('.new input[type=text]').keypress(function (e) {
      var key = e.which;
      if (key == 13) {
        set();
        return false;
      }
    });

    jQuery(".new a").click(function() {
      set();
    });

    jQuery(".message").delay(1500).slideUp();

    jQuery(document).on('keyup keydown', function(e) {
      shifted = e.shiftKey;
    });

    jQuery(".finish a").click(function() {
      save();
    });

    win.scroll(function() {
      if (jQuery(document).height() - win.height() == win.scrollTop()) {
        more();
      }
    });
  });

  function set() {
    jQuery(".tags").prepend('<li data-flag="new">'+
      '<label for="new-tag-'+(jQuery("li[data-flag=new]").length+1)+'" class="button tag button-primary">'+
      '<input type="checkbox" name="new_tag[]" onchange="toggleTag(this)" id="new-tag-'+(jQuery("li[data-flag=new]").length+1)+'" value="'+jQuery(".new input[type=text]").val()+'" checked="checked" />'+
      jQuery(".new input[type=text]").val()+
      '</label>'+
      '</li>');
    jQuery(".new input[type=text]").val("");
  }

  function toggleTag(elem) {
    jQuery(elem).parent().toggleClass("button-primary");
  }

  function save() {
    var pos1    = 0;
    var pos2    = jQuery(".andromeda-grid .highlight").length;
    var tags    = new Array();
    var newTags = new Array();

    jQuery.each(jQuery(".tags li .button-primary"), function(i, elem) {
      if (jQuery(elem).attr("for").indexOf("new-tag") > -1) {
        newTags.push(jQuery(elem).find("input[type=checkbox]").val());
      } else {
        tags.push(jQuery(elem).find("input[type=checkbox]").val());
      }
    });

    function pool() {
      var IDs = new Array();

      if (pos2 - pos1 > maxed) {
        var limit = pos1 + maxed;
      } else {
        var limit = pos1 + (pos2 - pos1);
      }

      for (var i = pos1; i < limit; i++) {
        var _id = jQuery(".andromeda-grid .highlight")
          .eq(i)
          .find("label")
          .attr("for");
        IDs.push(_id.replace("img-", ""));
      }

      pos1 = limit;
      batchSend(newTags, tags, IDs);
    }

    function batchSend(newTags, tags, items) {
      jQuery.ajax({
        url  : ajaxurl,
        type : "post",
        data : {
          action  : "save_tags",
          new_tag : newTags,
          tag     : tags,
          image   : items},
        success : function(response) {
          if (pos2 - pos1 > 0) {
            pool();
          } else {
            jQuery(".aftermath").fadeIn("fast").delay(2000).fadeOut("fast");
          }
        }
      });
    }

    pool();
  }

  function more() {
    offset = offset + maxed;

    jQuery.ajax({
      url  : ajaxurl,
      type : "post",
      data : {
        offset : offset,
        action : "more_imgs"
      },
      success : function(response) {
        jQuery.each(response, function(i, item) {
          var img = new Image(103, 103);
          img.src = item.guid.replace('.jpg', '-150x150.jpg');
          img.alt = item.post_title;
          var label = document.createElement("label");
          label.setAttribute("for", "img-"+item.ID);
          label.appendChild(img);
          var input = document.createElement("input");
          input.type = "checkbox";
          input.id = "img-"+item.ID;
          input.name = "image[]";
          input.value = item.ID;
          input.addEventListener("change", function() {toggleImg(this);});
          var li = document.createElement("li");
          li.appendChild(label);
          li.appendChild(input);
          jQuery(".andromeda-grid").append(li);
        });
      }
    });
  }

  function toggleImg(elem) {
    jQuery(elem).parent().toggleClass("highlight");

    if (shifted) {
      var _i = jQuery(".highlight").index(jQuery(elem).parent(".highlight"));

      if (_i > 0) {
        var pos1 = jQuery(jQuery(".highlight")[_i - 1]).index();
        var pos2 = jQuery(elem).parent().index();
      } else {
        var pos1 = jQuery(elem).parent().index();
        var pos2 = jQuery(jQuery(".highlight")[1]).index();
      }

      for (var i = pos1 + 1; i < pos2; i++) {
        jQuery(".andromeda-grid li")
          .eq(i)
          .addClass("highlight")
          .find("input[type=checkbox]")
          .attr("checked", "checked");
      }
    }
  }
</script>
