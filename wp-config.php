<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'local');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Yf9I5ivs,PPYp[xriv5nX}sn[T$c7>Ep}Y*d-#]OH?i5sc=kj)bq3Ey0?T+>.0LM');
define('SECURE_AUTH_KEY',  'a.p.orIr6~V?Z=.mB)J9th`L;1T{=f7O_EH%(w;aFqA f)`h.-R$(`!5NBYw~R62');
define('LOGGED_IN_KEY',    '0&IpjV !o}-KcXTsW,zw8`1zNXYcW>6 B2p*J~5~2 gSAsLe-C.V3wL3JYNwq(e|');
define('NONCE_KEY',        'I5sd@XBh>3x>nDDWFxkR`Ii5qAE?@ja(4R<^xVHB^/_Q*wc{#e+vJ=,T`riwKbez');
define('AUTH_SALT',        '`H6t(<kG`CU1zr>~ZHS|^SR7rXhURA TO<2M7Z.{[}73{V&]+|__esDfeqfw`:|q');
define('SECURE_AUTH_SALT', '}@7oxTgg=_=[mC=?+y#1=?KjQ*vKQ%Ux f*$05[_PdI8;Pe-f2DR*!5ZL)=1<w;!');
define('LOGGED_IN_SALT',   'L7Fp!.`~f1YEAUdbB2:6~U*6+2Y:;V,vT7bq:rz~!3eI>}Z|YGCf_G9#uqa La<q');
define('NONCE_SALT',       ' sP@Q25sPhj#.qUQq19FVUbXtl9cf>,$Q;[Cs(%Tf*{B9`<7** )na{(_QAH2wE0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
